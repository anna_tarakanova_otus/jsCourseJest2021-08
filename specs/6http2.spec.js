import Login from '../framework/services/login.service';
import api from '../framework/services';
import Register from "../framework/services/register.service";

test('Пользователь может авторизоваться используя логин и пароль', async () => {
  const body = { username: 'demo', password: 'demo' };
  const r = await Login.post(body);
  expect(r.status).toEqual(200);
});

test('Пользователь может авторизоваться используя логин и невалидный пароль', async () => {
  const body = { username: 'demo', password: 'demo1' };
  const r = await Login.post(body);
  expect(r.status).toEqual(412);
});
test('Пользователь не может авторизоваться используя невалидный логин и пароль', async () => {
  const body = { username: 'demo1', password: 'demo1' };
  const r = await Login.post(body);
  expect(r.status).toEqual(412);
});

test.only('Пользователь может авторизоваться после регистрации', async () => {
// todo Сделать генерацию данных
  const body = { username: 'demo12', email: 'otusotus@5min.mail', password: 'demo12' };
  await api().Register().post(body);
  const r = await api().Login().post({ username: body.username, password: body.password });
  expect(r.status).toEqual(200);
});
