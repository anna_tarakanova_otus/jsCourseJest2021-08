import fetch from 'node-fetch';

/*
Проверяем API Airportgap
*/

test('get /airports works', async () => {
    const response = await fetch('https://airportgap.dev-tester.com/api/airports');
    expect(response.status).toEqual(200);
});

test('get /airports/:id works', async () => {
    const response = await fetch('https://airportgap.dev-tester.com/api/airports/KIX')
    expect(response.status).toEqual(200);
});

test('post /airports/distance works', async () => {
    const response = await fetch('https://airportgap.dev-tester.com/api/airports/distance?from=KIX&to=NRT',
        {
            method: 'POST',
        });
    expect(response.status).toEqual(200);
});

test('post /airports/token works', async () => {
    const response = await fetch('https://airportgap.dev-tester.com/api/airports/distance?from=KIX&to=NRT',
        {
            method: 'POST',
        });
    expect(response.status).toEqual(200);
});

test(`failed post /airports/token doesn't work`, async () => {
    const response = await fetch('https://airportgap.dev-tester.com/api/airports/distance?from=KIX',
        {
            method: 'POST',
        });
    expect(response.status).toEqual(422);
});


test('get /favorites works', async () => {
    const response = await fetch('https://airportgap.dev-tester.com/api/favorites',
        {
            headers: {Authorization: 'Token c9R9SVu1SY47kt3hH2GsokgQ'},
        });
    expect(response.status).toEqual(200);
});