
import {coolBall, newYear} from '../src/homework4';

describe('ДЗ 4', () => {

  test.each`
    name | expected
    ${"дедушка"} | ${"я от дедушки ушел"}
    ${"бабушка"} | ${"я от бабушки ушел"}
    ${"медведь"} | ${"я от медведя ушел"}
    ${"волк"} | ${"я от волка ушел"}
    ${"заяц"} | ${"я от зайца ушел"}
    ${"лиса"} | ${"а от лисы не уйдешь"}
    ${"чупакабра"} | ${"такого персонажа в сказке не было"}
    `(`if $name is passed $expected is returned`, ({name, expected}) => {
      expect(coolBall(name)).toBe(expected);
    });


  test.each`
    name | expected
    ${"Дед Мороз"} | ${"Дед Мороз! Дед Мороз! Дед Мороз!"}
    ${"Снегурочка"} | ${"Снегурочка! Снегурочка! Снегурочка!"}
    ${"Масленица"} | ${"уходите, мы вас не звали"}
    `(`зовем $name на новый год`, ({name, expected}) => {
      expect(newYear(name)).toBe(expected);
    });
});
