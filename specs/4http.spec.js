import fetch from 'node-fetch';
import supertest from 'supertest';
/*
Пробуем как отправляют запросы разные библиотеки
*/
test('get with node-fetch', async () => {
  const response = await fetch('https://api.nasa.gov/planetary/apod?api_key=Q5bz7IZm7gPtFsTe4mHdFhYBbEsmk23Ftq5nQJw6');
  expect(response.status).toEqual(200);
  const { status } = await fetch('https://api.nasa.gov/planetary/apod?api_key=Q5bz7IZm7gPtFsTe4mHdFhYBbEsmk23Ftq5nQJw6');
  expect(status).toEqual(200);
});

test('get with supertest', async () => {
  const response = await supertest('https://api.nasa.gov')
    .get('/planetary/apod?api_key=Q5bz7IZm7gPtFsTe4mHdFhYBbEsmk23Ftq5nQJw6')
    .set('Accept', 'application/json');
  expect(response.status).toEqual(200);
});

test('post with node-fetch', async () => {
  const r = await fetch('https://apichallenges.herokuapp.com/challenger',
    {
      method: 'POST',
      headers: { Accept: 'application/json' },
    });
  const token = r.headers['x-challenger'];
  const { status } = await fetch('https://apichallenges.herokuapp.com/challenges',
    {
      method: 'GET',
      headers: { 'X-CHALLENGER': token },
    });
  expect(status).toEqual(200);
});

test('post with supertest', async () => {
  const r = await supertest('https://apichallenges.herokuapp.com')
    .post('/challenger')
    .set('Accept', 'application/json');

  // console.log(r.header['x-challenger']);
  expect(r.status).toEqual(201);
});
