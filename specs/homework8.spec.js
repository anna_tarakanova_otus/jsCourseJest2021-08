import api from '../framework/services';

const accessKey = '77a20a9c075e9ab7a6ca8259d2919839';

test('check valid email', async () => {
    const response = await api().Mailboxes().check(accessKey, 'anna.tarakanova@mail.ru');
    expect(JSON.parse(response.text).format_valid).toEqual(true);
});

test.each`
    email | errorCode
    ${"anna.tarakanovamail.ru"} | ${211}
    ${"anna.tarakanova@"} | ${211}
    ${""} | ${210}
    `(`check invalid email $email for error code $errorCode`, async({email, errorCode}) => {
        const response = await api().Mailboxes().check(accessKey, email);
        const body = JSON.parse(response.text);
        expect(body.error.code).toEqual(errorCode);
    });

test('check access to api without accessKey', async () => {
    const response = await api().Mailboxes().check('', 'anna.tarakanova@mail.ru');
    const body = JSON.parse(response.text);
    expect(body.error.type).toEqual('missing_access_key');
});