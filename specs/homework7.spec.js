import api from '../framework/services';

/*
check API Airportgap with controllers
*/

test('get /airports works', async () => {
    const response = await api().Airports().get();
    expect(response.status).toEqual(200);
});

test('get /airports/:id works', async () => {
    const response = await api().Airports().getId('KIX')
    expect(response.status).toEqual(200);
});

test('get /airports/:id works with invalid value', async () => {
    const response = await api().Airports().getId('NNN')
    expect(response.status).toEqual(404);
});

test('get /airports/:id works with digit value', async () => {
    const response = await api().Airports().getId(1)
    expect(response.status).toEqual(404);
});

test('post /airports/distance works', async () => {
    const response = await api().Airports().postDistance('KIX','NRT');
    expect(response.status).toEqual(200);
});

test('post /airports/distance works with only airportA known', async () => {
    const response = await api().Airports().postDistance('KIX');
    expect(response.status).toEqual(422);
});

test('post /airports/distance works with no airports known', async () => {
    const response = await api().Airports().postDistance();
    expect(response.status).toEqual(422);
});

test('get /favorites works', async () => {
    const token = 'Token c9R9SVu1SY47kt3hH2GsokgQ';
    const response = await api().Favorites().get(token);
    expect(response.status).toEqual(200);
});

test('get /favorites doesnt work with invalid token', async () => {
    const token = 'Token c9R9SVu1SY47kt3hH2G000';
    const response = await api().Favorites().get(token);
    expect(response.status).toEqual(401);
});

test('post /favorites works', async () => {
    const token = 'Bearer token=c9R9SVu1SY47kt3hH2GsokgQ';
    const body = 'airport_id=JFK&note=My usual layover when visiting family';
    const response = await api().Favorites().post(token, body)
    expect(response.status).toEqual(200);
});