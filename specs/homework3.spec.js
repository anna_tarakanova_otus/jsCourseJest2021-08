import { fullTrim, nameIsValid } from '../src/homework3';

describe('Здесь могла бы быть ваша реклама', () => {

  /**
   * Допишите несколько unit тестов для функции (вспоминаем тест-дизайн),
   * которая удаляет пробелы и табуляторы в начале и конце строки.
   */

  test('it loads without error', () => {
    expect(fullTrim).toBeDefined();
    expect(typeof fullTrim).toBe('function');
  });

  test('Проверяем тримминг для пробела в начале слова', () => {
    expect(fullTrim(' Это домашка')).toEqual('Это домашка');
  });
  test('Проверяем тримминг для таба в начале слова', () => {
    expect(fullTrim(' Это домашка')).toEqual('Это домашка');
  });
  test('Проверяем тримминг для пробела в конце слова', () => {
    expect(fullTrim('Это домашка ')).toEqual('Это домашка');
  });
  test('Проверяем тримминг для таба в конце слова', () => {
    expect(fullTrim('Это домашка  ')).toEqual('Это домашка');
  });

  /**
   * Напишите параметризированный unit (describe.each`table`) тест для функции,
   * которая проверяет валидность кличек котиков.
   * Кличка считается валидной, если она соответствует следующим условиям:
   * 1) Кличка содержит минимум два символа;
   * 2) Кличка не пустая;
   * 3) Кличка не содержит пробелов.

   Обратите внимание, в примере приведен обычный тест.
   Параметризированный тест - https://jestjs.io/docs/en/api#testeachtablename-fn-timeout
   */
  describe.each`
    testName | expected
    ${'a'} | ${false}
    ${'Snow Ball'} | ${false}
    ${'Snowball'} | ${true}
    `('name is valid?', ({ testName, expected }) => {
    expect(nameIsValid(testName)).toBe(expected);
  });
});
