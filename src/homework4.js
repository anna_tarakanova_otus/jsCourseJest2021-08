/**
 * Это функция "Колобок" для ДЗ-4
 * @param {string} name
 * @returns {string}
 */

export const coolBall = (name) => {
    switch (name) {
        case "дедушка":
            return "я от дедушки ушел";
            break;
        case "бабушка":
            return "я от бабушки ушел";
            break;
        case "медведь":
            return "я от медведя ушел";
            break;
        case "волк":
            return "я от волка ушел";
            break;
        case "заяц":
            return "я от зайца ушел";
            break;
        case "лиса":
            return "а от лисы не уйдешь";
            break;
        default:
            return "такого персонажа в сказке не было";
    }
};

export const newYear = (name) => {
    if (name === "Дед Мороз" || name === "Снегурочка") {
        return `${name + "! " + name + "! " + name +"!"}`;
    }
    else {
        return "уходите, мы вас не звали";
    }
}
