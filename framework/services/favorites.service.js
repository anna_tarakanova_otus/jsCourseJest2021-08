import supertest from "supertest";
import urls from "../config";

const Favorites = {
    get: async (token) => {
        return await supertest(urls.airportgap)
            .get('/favorites')
            .set('Authorization', token);
        },

    post: async (token, body) => {
        return await supertest(urls.airportgap)
            .get('/favorites')
            .set('Authorization', token)
            .send(body);
    },
}
export default Favorites;