import supertest from "supertest";
import urls from "../config";

const Mailboxes = {
    check: async (accessKey, email) => {
        return await supertest(urls.mailboxlayer)
            .get('/check?' + 'access_key=' + accessKey + '&email=' + email + '&smtp=1&format=1');
    },
}

export default Mailboxes;