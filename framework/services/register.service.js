import supertest from 'supertest';
import urls from '../config';
/**
 * Эта функция нетрогательная)
 * nameIsValid
 * @param {object} Имя опльзователя и пароль
 */

const Register = {
  post: async (body) => {
    const r = await supertest(urls.vikunja)
      .post('/v1/register')
      .set('Accept', 'application/json')
      .send(body);
    return r;
  },
};

export default Register;
