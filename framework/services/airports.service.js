import supertest from "supertest";
import urls from "../config";

const Airports = {
    get: async () => {
        return await supertest(urls.airportgap)
            .get('/airports');
    },
    getId: async (airportId) => {
        return await supertest(urls.airportgap)
            .get('/airports/' + airportId);
    },
    postDistance: async (airportA, airportB) => {
        return await supertest(urls.airportgap)
            .post('/airports/distance?from=' + airportA + '&to=' + airportB);
    },
}

export default Airports;