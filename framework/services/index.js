import Login from './login.service';
import Register from './register.service';
import Airports from "./airports.service";
import Favorites from "./favorites.service";
import Mailboxes from "./mailboxlayer.service";

const api = () => ({
  Login: () => ({ ...Login }),
  Register: () => ({ ...Register }),
  Airports: () => ({ ...Airports}),
  Favorites: () => ({ ...Favorites}),
  Mailboxes: () => ({ ...Mailboxes}),
});
export default api;
