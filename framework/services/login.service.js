import supertest from 'supertest';
import urls from '../config';
/**
 * Эта функция нетрогательная)
 * nameIsValid
 * @param {object} Имя опльзователя и пароль
 */

const Login = {
  post: async (body) => {
    const r = await supertest(urls.vikunja)
      .post('/v1/login')
      .set('Accept', 'application/json')
      .send(body);
    return r;
  },
  get: async (body) => {
    const r = await supertest(urls.vikunja)
      .get('/v1/login')
      .set('Accept', 'application/json')
      .send(body);
    return r;
  },
  postId: async (body, id) => {
    const r = await supertest(urls.vikunja)
      .post(`/v1/login/+ ${id}`)
      .set('Accept', 'application/json')
      .send(body);
    return r;
  },
};

export default Login;

/* const LoginVikunja = function () {
  this.post = async function (body) {
    const r = await supertest(urls.vikunja)
      .post('/v1/login')
      .set('Accept', 'application/json')
      .send(body);
    return r;
  };
};

export default LoginVikunja; */
