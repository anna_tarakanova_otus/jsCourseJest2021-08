import faker from 'faker';

function deepCopy(example) {
  return JSON.parse(JSON.stringify(example));
};

const BuildUser = function () {
  this.get = function () {
    const data = {
      username: faker.internet.userName(),
      email: faker.internet.email(),
      password: faker.internet.password(),
    };
    return deepCopy(data);
  };
};

export default BuildUser;
