const urls = {
  vikunja: 'https://try.vikunja.io/api',
  airportgap: 'https://airportgap.dev-tester.com/api',
  mailboxlayer: 'http://apilayer.net/api',
};

export default urls;
