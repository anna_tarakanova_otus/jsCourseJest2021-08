module.exports = {
  testEnvironment: 'node',
  reporters: [
    'default',
    'jest-html-reporters',
    [
      'jest-stare',
      {
        resultDir: 'results/jest-stare',
        reportTitle: 'jest-stare!',
        additionalResultsProcessors: [
          'jest-junit',
        ],
      },
    ],
  ],
  moduleFileExtensions: ['js', 'json'],
  transform: {
    '^.+\\.jsx?$': 'babel-jest',
  },
  testMatch: ['**/specs/*.spec.*'],
  globals: {
    testTimeout: 50000,
  },
  verbose: true,
};
